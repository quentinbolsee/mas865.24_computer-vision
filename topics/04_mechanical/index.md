---
layout: default
title: Mechanical Digitizing
nav_order: 5
mathjax: true
---

# Mechanical Digitizers
{: .no_toc}

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

# Atomic Force Microscopy
<p>Atomic Force Microscopy(AFM) or Scanning Probe Microscopy(SPM)</p>
<p>~ nanometer </p>

<img src="images/atomic_force_microscope_block_diagram.png" alt="AFM">
<iframe src="https://www.youtube.com/embed/8gCf1sEn0UU" frameborder="0" allowfullscreen=""></iframe>

<iframe width="500" height="400" src="https://youtube.com/embed/F2d4Hq1CBdg" frameborder="0" allowfullscreen=""></iframe>

<p>Image modes</p>
<p>Contact mode, Tapping mode, Non-contact mode</p>

<img src="images/scanning_probe_microscopy.png" alt="modes">

Non-contact Mode: Oscillates above the adsorbed fluid layer on the surface during scanning

<ul> <li> <p>References</p> <p><a href="https://www.highspeedscanning.com/">High Speed Scanning with SPM</a><br> <a href="https://www.nanosurf.com/en/support/afm-operating-principle">How does AFM work</a><br> <a href="https://www.nanoworld.com/?gclid=CjwKCAjw-e2EBhAhEiwAJI5jgwRZEaR0sOLoeR1_gK5ZF48CKQmBWZe04e8AGIwI9kFXPUt0MjP1_hoC5JoQAvD_BwE">Nanoworld</a><br></p> </li> <li> <p>AFM Probe</p> </li> </ul>
<p><a href="https://www.nanoandmore.com/AFM-Probe-USC-F0.3-k0.3">AFM Probe USC-F0.3-k0.3-10, image above, $980 per 10</a>, <a href="https://www.budgetsensors.com/?gclid=CjwKCAjwnPOEBhA0EiwA609RefDrTdyqhnt17UPTZEnNslIPfxV10pRReItfPxQdJLqJY78li9KlWBoCT_AQAvD_BwE">Budgetsensors</a> <a href="https://www.agilent.com/cs/library/slidepresentation/Public/AFM%20Probe%20ManufacturingNanoworld_tip_technologyPRussell07.pdf">Fabrication steps of AFM Probes</a></p>

<p><a href="https://www.sciencedirect.com/science/article/pii/S0304399118301797">Tip wear and tip breakage in high-speed atomic force microscopes</a></p>


# Touch Probes

<a href="https://www.marposs.com/eng/application/technologies-touch-probes#:~:text=The%20working%20principle%20is%20quite,called%20the%20probe%20pre%2Dtravel.">Touch Probes</a>

<iframe width="560" height="315" src="https://www.youtube.com/embed/vJdcbhGa7Jk?si=YeFE7Buf8KPLohNa&amp;start=18" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<p>Detecting contact by measuring resistance between the balls and rollers.</p>

<img src="images/probe_light.png" alt="ProbeLight">

<a href="https://www.blum-novotest.com/en/products/measuring-components/workpiece-probes/tc50-tc60.html">BLUM, The trigger signal is generated via shading of a miniature light barrier</a>



Bridge Coordinate Measuring Machines (CMM)
<img src="images/bridge_cmm.jpg" alt="Bridge CMM">


CNC Machine -&gt; Workspace alignment/present setting / workpiece measurement, digitizing or inspection
<iframe width="560" height="315" src="https://www.youtube.com/embed/IcVv1So1n_8?si=s9orrO0G6nlK0ohr&amp;start=8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>




Optical
Optically tracked touch probes
<a href="https://www.gom.com/nl/metrology-systems/gom-taster.html">GOM</a>

<img src="images/gom_touch_probe.jpg" alt="GOM touch probe">

<iframe width="560" height="315" src="https://www.youtube.com/embed/wt4PLgbwxQI?si=A0yDBFneMn1wFotE&amp;start=98" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>




### Digitizer Arms

Passive Robot Arm<br>
<img src="images/portable_robot_arm.jpeg" alt="Portable Robot Arm">

<img src="images/gage_faro_arm.png" alt="Gage Faro Arm"><br>


Typically, robotic arms used in manufacturing offer working volumes ranging from 4 to 12 feet, with radial reach extending from 2 to 6 feet. These arms predominantly come in six- or seven-axis configurations, depending on the specific requirements.
Fixed Coordinate Measuring Machines (CMMs) excel in providing extreme precision, surpassing that of portable CMM arms.

### DIY

Universal Robots
<img src="images/universal_robot.jpg" alt="Universal Robot"><br>

You can approximate the functionality of a digitizer arm using any robot arm, by reading the axis rotations and reconstructing the location of the end effector in 3D space. Typically, read-outs from a robot arm will be in [Euler Angles](https://en.wikipedia.org/wiki/Euler_angles).

DIY Projects

<p><a href="http://blog.dzl.dk/2018/08/21/3d-digitizer/">Digitizer</a></p>
<p><a href="https://www.instructables.com/CNC-Manual-Touch-Probe/">Touch Probe</a></p>
<p><a href="http://www.homemetalshopclub.org/news/12/presentation_1202.pdf">Building a Digitizing Probe</a></p>


# Large Scale


Beyond inspection in manufacturing contexts, Frank Gehry was the first architect to incorporate mechanical digitizing into the process of form making:
<img src="images/gehry_scanning_exterior_model.jpg" alt="Source: Gehry Technologies">

The technical complexities behind this feat are documented thoroughly in Dennis Shelden's 2002 MIT Dissertation:
[Digital Surface Representation and the Constructability of Gehry's Architecture](https://dspace.mit.edu/handle/1721.1/16899)
<img src="images/shelden_digital_surface_representation_p299.jpg" alt="Source: Dennis Shelden">

The precursor to MIT's Stata Center, Gehry's 2000 Museum of Pop Culture in Seattle was the first to test translating this digitization workflow into full scale construction:

<img src="images/mopop_steel_ribs.jpg" alt="Hoffman Construction">
<img src="images/gehry_museum_of_pop_culture_2000_seattle.jpg" alt="Gehry Museum of Pop Culture">


### Digital Templating

Wire based templating devices take measurements from rotary encoders

<iframe width="560" height="315" src="https://www.youtube.com/embed/9dd1w9VAxw0?si=9kcd6ggq5RGd8YOL&amp;start=67" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Survey Prisms

A survey prism is a glass reflector that is used in surveying and engineering to reflect the Electronic Distance Measurement (EDM) beam of a total station.

<img src="images/survey_prism_railroad.jpg" alt="Survey Prism">

Site conditions usually vary so greatly that the global coordinate system is not relevant, rather the local coordinate system between particular elements.

[SumPoint](https://www.sumpoint.io/#services)

<img src="images/robot_surveying.jpg" alt="Maria Yablonina and James Coleman">

[Small Robots and Big Projects](https://papers.cumincad.org/data/works/att/acadia21_462.pdf), Maria Yablonina and James Coleman, ACADIA 2021
