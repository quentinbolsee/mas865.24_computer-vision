---
title: Home
layout: home
nav_order: 1
---

# Computer Vision and scanning processes

## [Camera basics]({{ '/topics/01_cameras' | relative_url }})

## [Passive Scanning]({{ '/topics/02_passive' | relative_url }})

## [Active Scanning]({{ '/topics/03_active' | relative_url }})

## [Mechanical Digitizing]({{ '/topics/04_mechanical' | relative_url }})

<!-- ## [Image processing]({{ '/topics/05_processing' | relative_url }}) -->
